#@field IPADDR
#@type STRING
#IP or hostname of the TCP endpoint.
#
#@field IPPORT
#@type INTEGER
#IP port of the TCP endpoint.
#
#@field PREFIX
#@type STRING
#Prefix for EPICS PVs.
#
#@field SCAN
#@type STRING
#Scan rate for PVs.

require pvaSrv,0+

# just use the prefix as the portname
epicsEnvSet("PORTNAME", "$(PREFIX)")

#Specifying the TCP endpoint and port name
drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

#Load your database defining the EPICS records
dbLoadRecords(ne1600.db, "P=$(PREFIX), PORT=$(PORTNAME), ADDR=$(IPPORT), SCAN=$(SCAN)")
